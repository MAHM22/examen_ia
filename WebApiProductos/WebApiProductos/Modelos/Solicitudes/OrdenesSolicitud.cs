﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiProductos.Modelos.Solicitudes
{
    public class OrdenesSolicitud
    {
        public int Id { get; set; } 

        public int Mesa { get; set; }

        public int IdProducto { get; set; }

        public int Cantidad { get; set; }

        public string Orden { get; set; }

        public int Estatus { get; set; }
    }
}
