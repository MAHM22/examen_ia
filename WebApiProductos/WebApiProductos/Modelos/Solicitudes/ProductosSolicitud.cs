﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiProductos.Modelos.Solicitudes
{
    public class ProductosSolicitud
    {

        public int Id { get; set; }

        public string Sku { get; set; }

        public string Nombre { get; set; }

        public int Stock { get; set; }

        public int Minimo { get; set; }


    }
}
