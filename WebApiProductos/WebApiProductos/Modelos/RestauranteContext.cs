﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace WebApiProductos.Modelos
{
    public partial class RestauranteContext : DbContext
    {
        public RestauranteContext()
        {
        }

        public RestauranteContext(DbContextOptions<RestauranteContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CatEstatus> CatEstatus { get; set; }
        public virtual DbSet<CatMensaje> CatMensajes { get; set; }
        public virtual DbSet<Ordene> Ordenes { get; set; }
        public virtual DbSet<Producto> Productos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=LAPTOP-MMV9MQM3;Database=Restaurante; Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");

            modelBuilder.Entity<CatEstatus>(entity =>
            {
                entity.ToTable("CatEstatus");

                entity.Property(e => e.Estatus)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<CatMensaje>(entity =>
            {
                entity.Property(e => e.Mensaje)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Ordene>(entity =>
            {
                entity.Property(e => e.Orden)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsFixedLength(true);

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.Ordenes)
                    .HasForeignKey(d => d.IdProducto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Ordenes_Ordenes");
            });

            modelBuilder.Entity<Producto>(entity =>
            {
                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sku)
                    .HasMaxLength(10)
                    .IsFixedLength(true);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
