﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApiProductos.Modelos
{
    public partial class Ordene
    {
        public int Id { get; set; }
        public int Mesa { get; set; }
        public int IdProducto { get; set; }
        public int Cantidad { get; set; }
        public string Orden { get; set; }
        public int Estatus { get; set; }

        public virtual Producto IdProductoNavigation { get; set; }
    }
}
