﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApiProductos.Modelos
{
    public partial class CatEstatus
    {
        public int Id { get; set; }
        public string Estatus { get; set; }
    }
}
