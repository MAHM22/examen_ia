﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApiProductos.Modelos
{
    public partial class CatMensaje
    {
        public int Id { get; set; }
        public string Mensaje { get; set; }
    }
}
