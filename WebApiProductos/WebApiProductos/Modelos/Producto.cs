﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApiProductos.Modelos
{
    public partial class Producto
    {
        public Producto()
        {
            Ordenes = new HashSet<Ordene>();
        }

        public int Id { get; set; }
        public string Sku { get; set; }
        public string Nombre { get; set; }
        public int? Stock { get; set; }
        public int? Minimo { get; set; }

        public virtual ICollection<Ordene> Ordenes { get; set; }
    }
}
