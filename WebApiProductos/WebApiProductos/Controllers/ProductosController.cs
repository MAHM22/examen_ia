﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiProductos.Modelos.Respuesta;
using WebApiProductos.Modelos.Solicitudes;
using WebApiProductos.Modelos;

namespace WebApiProductos.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductosController : ControllerBase
    {
        [HttpGet("{Id?}")]
        public IActionResult Get(int? Id)
        {
            Respuesta ORespuesta = new Respuesta();

            try
            {
                //Se el parametro Id es diferente de null buscamos un Producto en especifico
                if (Id != null)
                {
                    using (RestauranteContext db = new RestauranteContext())
                    {

                        var lst = db.Productos.Find(Id);
                        var Mensaje = db.CatMensajes.Find(1);
                        ORespuesta.Exito = Mensaje.Id;
                        ORespuesta.Mensaje = Mensaje.Mensaje;
                        ORespuesta.Data = lst;
                    }
                }
                //De lo contrario obtenemos toda la lista
                else
                {
                    using (RestauranteContext db = new RestauranteContext())
                    {
                        
                        var lst = db.Productos.ToList();
                        var Mensaje = db.CatMensajes.Find(1);
                        ORespuesta.Exito = Mensaje.Id;
                        ORespuesta.Mensaje = Mensaje.Mensaje;
                        ORespuesta.Data = lst;
                    }
                }

            }
            catch (Exception ex)
            {
                
                ORespuesta.Mensaje = ex.Message;
            }



            return Ok(ORespuesta);


        }

        // El metodo Post crea el nuevo producto
        [HttpPost]
        public IActionResult Add(ProductosSolicitud model)
        {
            Respuesta ORespuesta = new Respuesta();

            try
            {
                using (RestauranteContext db = new RestauranteContext())
                {
                    // con ayuda Linq y expresiones Lambda validamos si el producto ya existe
                    if (db.Productos.Any(o => o.Sku == model.Sku))
                    {
                        var Mensaje = db.CatMensajes.Find(6);
                        ORespuesta.Mensaje = Mensaje.Mensaje;
                        ORespuesta.Exito = Mensaje.Id;
                    }
                    else
                    {
                        Producto oProducto = new Producto();
                        oProducto.Sku = model.Sku;
                        oProducto.Nombre = model.Nombre;
                        oProducto.Stock = model.Stock;
                        oProducto.Minimo = model.Minimo;


                        db.Productos.Add(oProducto);
                        db.SaveChanges();
                        var Mensaje = db.CatMensajes.Find(5);
                        ORespuesta.Mensaje = Mensaje.Mensaje;
                        ORespuesta.Exito = Mensaje.Id;

                      
                    }
                  

                }
            }
            catch (Exception ex)
            {
                ORespuesta.Mensaje = ex.Message;
            }



            return Ok(ORespuesta);


        }

        //El Metodo Put funciona para actualizar los Productos
        [HttpPut]
        public IActionResult Edit(ProductosSolicitud model)
        {
            Respuesta ORespuesta = new Respuesta();

            try
            {
                using (RestauranteContext db = new RestauranteContext())
                {

                    Producto oProducto = db.Productos.Find(model.Id);
                    oProducto.Sku = model.Sku;
                    oProducto.Nombre = model.Nombre;
                    oProducto.Stock = model.Stock;
                    oProducto.Minimo = model.Minimo;
                    db.Entry(oProducto).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    db.SaveChanges();       
                    var Mensaje = db.CatMensajes.Find(4);
                    ORespuesta.Exito = Mensaje.Id;
                    ORespuesta.Mensaje = Mensaje.Mensaje;

                }
            }
            catch (Exception ex)
            {
                ORespuesta.Mensaje = ex.Message;
            }



            return Ok(ORespuesta);





        }

        //El metodo Delete de la API recibe un parametro del Productos a eliminar
        [HttpDelete("{Id}")]
        public IActionResult Delete(int Id)
        {
            Respuesta ORespuesta = new Respuesta();

            try
            {
                using (RestauranteContext db = new RestauranteContext())
                {

                    Producto oProducto = db.Productos.Find(Id);
                    
                    db.Remove(oProducto);
                    db.SaveChanges();
                    var Mensaje = db.CatMensajes.Find(3);
                    ORespuesta.Exito = Mensaje.Id;
                    ORespuesta.Mensaje = Mensaje.Mensaje;



                }
            }
            catch (Exception ex)
            {
                ORespuesta.Mensaje = ex.Message;
            }



            return Ok(ORespuesta);


        }

    }

    }