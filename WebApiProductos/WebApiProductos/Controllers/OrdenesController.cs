﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiProductos.Modelos.Respuesta;
using WebApiProductos.Modelos;
using WebApiProductos.Modelos.Solicitudes;

// @nuget: Z.EntityFramework.Classic


namespace WebApiProductos.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdenesController : ControllerBase
    {

        [HttpGet("{Order?}/{Estatus?}")]
        public IActionResult Get(string? Order, string? estatus)
        {

            Respuesta ORespuesta = new Respuesta();

            try
            {
                //Se el parametro Id es diferente de null buscamos la Orden en especifico
                if (Order != null)
                {
                    using (RestauranteContext db = new RestauranteContext())
                    {

                        var lst = db.Ordenes.Join(db.Productos, // la tabla de origen de la combinación interna
                                  post => post.IdProducto,        // Seleccione la clave principal (la primera parte de la cláusula "on" en una declaración sql "join")
                                  meta => meta.Id,   // Seleccione la clave externa (la segunda parte de la cláusula "on")
                                 (post, meta) => new { Post = post, Meta = meta }) // seleccion
                            .Join(db.CatEstatus,
                            order => order.Post.Estatus,
                            est => est.Id,
                            (post,est)=> new {
                            IdOrden = post.Post.Id,
                            Mesa= post.Post.Mesa,
                            Producto = post.Meta.Nombre,
                            Cantidad = post.Post.Cantidad,
                            Orden = post.Post.Orden,
                            Estatus = est.Estatus
                            })
                            .Where(postAndMeta => postAndMeta.Orden == Order).ToList();    // Con ToList haces que no se tenga problema al tratar de leer como un objeto Json

                      
                        var Mensaje = db.CatMensajes.Find(9);
                        ORespuesta.Exito = Mensaje.Id;
                        ORespuesta.Mensaje = Mensaje.Mensaje;
                        ORespuesta.Data = lst;
                    }
                }
                //De lo contrario obtenemos toda la lista
                else if(estatus != null)
                {
                    using (RestauranteContext db = new RestauranteContext())
                    {

                        var lst = db.Ordenes.Join(db.Productos, // la tabla de origen de la combinación interna
                                   post => post.IdProducto,        // Seleccione la clave principal (la primera parte de la cláusula "on" en una declaración sql "join")
                                   meta => meta.Id,   // Seleccione la clave externa (la segunda parte de la cláusula "on")
                                  (post, meta) => new { Post = post, Meta = meta }) // selección
                             .Join(db.CatEstatus,
                             order => order.Post.Estatus,
                             est => est.Id,
                             (post, est) => new {
                                 IdOrden = post.Post.Id,
                                 Mesa = post.Post.Mesa,
                                 Producto = post.Meta.Nombre,
                                 Cantidad = post.Post.Cantidad,
                                 Orden = post.Post.Orden,
                                 Estatus = est.Estatus
                             })
                             .Where(postAndMeta => postAndMeta.Estatus == estatus).OrderBy(c => c.Orden).ToList().GroupBy(ord=> ord.Orden).ToList()
                             .Select(fl => new { Ordenes = fl.Key, objects = fl.Select( x => new {x.IdOrden, x.Mesa, x.Producto, x.Cantidad, x.Orden, x.Estatus }).ToList() }).ToList() ;    // Con ToList haces que no se tenga problema al tratar de leer como un objeto Json
                        var Mensaje = db.CatMensajes.Find(9);
                        ORespuesta.Exito = Mensaje.Id;
                        ORespuesta.Mensaje = Mensaje.Mensaje;
                        ORespuesta.Data = lst;
                    }
                }
                else {
                    using (RestauranteContext db = new RestauranteContext())
                    {

                        var lst = db.Ordenes.Join(db.Productos, // la tabla de origen de la combinación interna
                                   post => post.IdProducto,        // Seleccione la clave principal (la primera parte de la cláusula "on" en una declaración sql "join")
                                   meta => meta.Id,   // Seleccione la clave externa (la segunda parte de la cláusula "on")
                                  (post, meta) => new { Post = post, Meta = meta }) // selección
                             .Join(db.CatEstatus,
                             order => order.Post.Estatus,
                             est => est.Id,
                             (post, est) => new {
                                 IdOrden = post.Post.Id,
                                 Mesa = post.Post.Mesa,
                                 Producto = post.Meta.Nombre,
                                 Cantidad = post.Post.Cantidad,
                                 Orden = post.Post.Orden,
                                 Estatus = est.Estatus
                             })
                             .OrderBy(c => c.Orden).ToList().GroupBy(ord => ord.Orden).ToList()
                             .Select(fl => new { Ordenes = fl.Key, objects = fl.Select(x => new { x.IdOrden, x.Mesa, x.Producto, x.Cantidad, x.Orden, x.Estatus }).ToList() }).ToList();    // Con ToList haces que no se tenga problema al tratar de leer como un objeto Json
                        var Mensaje = db.CatMensajes.Find(9);
                        ORespuesta.Exito = Mensaje.Id;
                        ORespuesta.Mensaje = Mensaje.Mensaje;
                        ORespuesta.Data = lst;
                    }
                }

            }
            catch (Exception ex)
            {

                ORespuesta.Mensaje = ex.Message;
            }

            return Ok(ORespuesta);
        }

        // El metodo Post crea la nueva Orden
        [HttpPost]
        public IActionResult Add(OrdenesSolicitud model)
        {
            Respuesta ORespuesta = new Respuesta();
            //Utilizamos esta variables de tipo entero para guardar la cantidad de productos que se van descontar del stock
            int cantidad = model.Cantidad;
            try
            {
                using (RestauranteContext db = new RestauranteContext())
                {
                    if (db.Productos.Any(o => o.Id == model.IdProducto && o.Stock > 0 && o.Stock >= model.Cantidad))
                    {

                        Ordene oOrdenes = new Ordene();
                        Producto oProductos = new Producto();
                        oOrdenes.IdProducto = model.IdProducto;
                        oOrdenes.Mesa = model.Mesa;
                        oOrdenes.Orden = model.Orden;
                        oOrdenes.Cantidad = model.Cantidad;
                        oOrdenes.Estatus = model.Estatus;

                        

                        db.Ordenes.Add(oOrdenes);
                        db.SaveChanges();
                        var Mensaje = db.CatMensajes.Find(8);
                        ORespuesta.Mensaje = Mensaje.Mensaje;
                        ORespuesta.Exito = Mensaje.Id;



                        // con ayuda de Entity.Extension  y expresiones Lamba actualizamos el stock
                        db.Productos.Where(d => d.Id == model.IdProducto).UpdateFromQuery(x => new Producto() { Stock = (x.Stock - cantidad ) });




                    }
                    else
                    {
                            var Mensaje = db.CatMensajes.Find(2);
                        ORespuesta.Mensaje = Mensaje.Mensaje;
                        ORespuesta.Exito = Mensaje.Id;
                    }

                }
                
            }
            catch (Exception ex)
            {
                ORespuesta.Mensaje = ex.Message;
            }



            return Ok(ORespuesta);


        }

        //El Metodo Put funciona para actualizar las Ordenes
        [HttpPut]
        public IActionResult Edit(OrdenesSolicitud model)
        {
            Respuesta ORespuesta = new Respuesta();

            try
            {
                using (RestauranteContext db = new RestauranteContext())
                {

                    db.Ordenes.Where(d=> d.Orden == model.Orden).UpdateFromQuery(x => new Ordene { Estatus = model.Estatus });



                    
                        var Mensaje = db.CatMensajes.Find(10);
                        ORespuesta.Exito = Mensaje.Id;
                        ORespuesta.Mensaje = Mensaje.Mensaje;
                    }

                
            }
            catch (Exception ex)
            {
                ORespuesta.Mensaje = ex.Message;
            }



            return Ok(ORespuesta);





        }
            
    }
}
