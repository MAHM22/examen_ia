import { Component, OnInit } from '@angular/core';
import { OrdenesDetallesService } from '../Servicios/ordenes-detalles.service';
import { OrdenesDetalles } from '../Servicios/ordenes-detalles.model';
import {MatButtonModule} from '@angular/material/button';

@Component({
  selector: 'app-ordenes-detalles',
  templateUrl: './ordenes-detalles.component.html',
  styles: [
  ]
})
export class OrdenesDetallesComponent implements OnInit {

  constructor(public servicio:  OrdenesDetallesService  ) { }
  lts: any =  [];
  lts2: any =  [];
  lts3: any =  [];
  lts4: any =  [];
  lts5: any =  [];
  ngOnInit(): void {
    
    this.servicio.ActualizarLista("Completada").subscribe(res => {
      this.lts = res;
      console.log(this.lts); 
      
      
      });
      
      this.servicio.ActualizarLista("Pendiente").subscribe(res => {
        this.lts2 = res;
        console.log(this.lts); 
        
        
        });

      this.servicio.ActualizarLista("En Proceso").subscribe(res => {
          this.lts3 = res;
          console.log(this.lts); 
          
          
          });

      this.servicio.ActualizarLista("Entregada").subscribe(res => {
            this.lts4 = res;
            console.log(this.lts); 
            
            
            });

      this.servicio.ActualizarLista("Cancelada").subscribe(res => {
              this.lts5 = res;
              console.log(this.lts); 
              
              
              });
  
  }
   
  UpdateProceso(item: any=[]){
    
  //pasamos la clave de estatus a nunero ya que asi lo recibe el servicio
  item.estatus = 2;
  item.orden = item.ordenes;
  this.servicio.ActualizarEstatus(item).subscribe(res =>{
  this.lts = res;
  console.log(this.lts);
  alert("se actualizo orden " + item.ordenes);
  this.ngOnInit();
  });
   
  }

  UpdateCompletada(item: any=[]){
    
    //pasamos la clave de estatus a nunero ya que asi lo recibe el servicio
    item.estatus = 3;
    item.orden = item.ordenes;
    this.servicio.ActualizarEstatus(item).subscribe(res =>{
    this.lts = res;
    console.log(this.lts);
    alert("se actualizo orden " + item.ordenes);
    this.ngOnInit();
    });
     
    }

    UpdateEntregada(item: any=[]){
    
      //pasamos la clave de estatus a nunero ya que asi lo recibe el servicio
      item.estatus = 4;
      item.orden = item.ordenes;
      this.servicio.ActualizarEstatus(item).subscribe(res =>{
      this.lts = res;
      console.log(this.lts);
      alert("se actualizo orden " + item.ordenes);
      this.ngOnInit();
      });
       
      }

      UpdateCancelada(item: any=[]){
    
        //pasamos la clave de estatus a nunero ya que asi lo recibe el servicio
        item.estatus = 5;
        item.orden = item.ordenes;
        this.servicio.ActualizarEstatus(item).subscribe(res =>{
        this.lts = res;
        console.log(this.lts);
        alert("se actualizo orden " + item.ordenes);
        this.ngOnInit();
        });
         
        }

}
