import { Injectable } from '@angular/core';
import { OrdenesDetalles } from './ordenes-detalles.model';
import { HttpClient}  from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class OrdenesDetallesService {
  header: Headers;

  constructor(private http: HttpClient) { }   
  
  readonly baseURL='http://localhost:62136/api/Ordenes'
  formData:OrdenesDetalles = new OrdenesDetalles();


    ActualizarLista(estatus: string)
  {
  
    return this.http.get<OrdenesDetalles[]>(this.baseURL+"/ /"+estatus) 
  }
  
  ActualizarEstatus(body:any = [])
  {
    return this.http.put(this.baseURL,body) 
  }

}
