USE [master]
GO
/****** Object:  Database [Restaurante]    Script Date: 13/07/2021 04:03:58 a. m. ******/
CREATE DATABASE [Restaurante]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Restaurante', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Restaurante.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Restaurante_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Restaurante_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Restaurante] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Restaurante].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Restaurante] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Restaurante] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Restaurante] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Restaurante] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Restaurante] SET ARITHABORT OFF 
GO
ALTER DATABASE [Restaurante] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Restaurante] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Restaurante] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Restaurante] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Restaurante] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Restaurante] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Restaurante] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Restaurante] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Restaurante] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Restaurante] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Restaurante] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Restaurante] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Restaurante] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Restaurante] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Restaurante] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Restaurante] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Restaurante] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Restaurante] SET RECOVERY FULL 
GO
ALTER DATABASE [Restaurante] SET  MULTI_USER 
GO
ALTER DATABASE [Restaurante] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Restaurante] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Restaurante] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Restaurante] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Restaurante] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Restaurante] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'Restaurante', N'ON'
GO
ALTER DATABASE [Restaurante] SET QUERY_STORE = OFF
GO
USE [Restaurante]
GO
/****** Object:  Table [dbo].[CatEstatus]    Script Date: 13/07/2021 04:03:59 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CatEstatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Estatus] [varchar](15) NOT NULL,
 CONSTRAINT [PK_CatEstatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CatMensajes]    Script Date: 13/07/2021 04:03:59 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CatMensajes](
	[Id] [int] IDENTITY(0,1) NOT NULL,
	[Mensaje] [varchar](50) NOT NULL,
 CONSTRAINT [PK_CatMensajes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ordenes]    Script Date: 13/07/2021 04:03:59 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ordenes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Mesa] [int] NOT NULL,
	[IdProducto] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Orden] [varchar](10) NOT NULL,
	[Estatus] [int] NOT NULL,
 CONSTRAINT [PK_Ordenes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 13/07/2021 04:03:59 a. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Productos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Sku] [nchar](10) NULL,
	[Nombre] [varchar](50) NULL,
	[Stock] [int] NULL,
	[Minimo] [int] NULL,
 CONSTRAINT [PK_Prodcutos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[CatEstatus] ON 

INSERT [dbo].[CatEstatus] ([Id], [Estatus]) VALUES (1, N'Pendiente')
INSERT [dbo].[CatEstatus] ([Id], [Estatus]) VALUES (2, N'En Proceso')
INSERT [dbo].[CatEstatus] ([Id], [Estatus]) VALUES (3, N'Completada')
INSERT [dbo].[CatEstatus] ([Id], [Estatus]) VALUES (4, N'Entregada')
INSERT [dbo].[CatEstatus] ([Id], [Estatus]) VALUES (5, N'Cancelada')
SET IDENTITY_INSERT [dbo].[CatEstatus] OFF
GO
SET IDENTITY_INSERT [dbo].[CatMensajes] ON 

INSERT [dbo].[CatMensajes] ([Id], [Mensaje]) VALUES (0, N'Error')
INSERT [dbo].[CatMensajes] ([Id], [Mensaje]) VALUES (1, N'Lista de Producto(s)')
INSERT [dbo].[CatMensajes] ([Id], [Mensaje]) VALUES (2, N'No se cuenta con Stock')
INSERT [dbo].[CatMensajes] ([Id], [Mensaje]) VALUES (3, N'Se borro el producto')
INSERT [dbo].[CatMensajes] ([Id], [Mensaje]) VALUES (4, N'Se actualizo el Producto')
INSERT [dbo].[CatMensajes] ([Id], [Mensaje]) VALUES (5, N'Se creo Producto')
INSERT [dbo].[CatMensajes] ([Id], [Mensaje]) VALUES (6, N'Este Producto ya existe')
INSERT [dbo].[CatMensajes] ([Id], [Mensaje]) VALUES (7, N'Ya existe ese Producto')
INSERT [dbo].[CatMensajes] ([Id], [Mensaje]) VALUES (8, N'Se Creo nueva Orden')
INSERT [dbo].[CatMensajes] ([Id], [Mensaje]) VALUES (9, N'Lista de Ordene(s)')
INSERT [dbo].[CatMensajes] ([Id], [Mensaje]) VALUES (10, N'Se actualizo Orden')
SET IDENTITY_INSERT [dbo].[CatMensajes] OFF
GO
SET IDENTITY_INSERT [dbo].[Ordenes] ON 

INSERT [dbo].[Ordenes] ([Id], [Mesa], [IdProducto], [Cantidad], [Orden], [Estatus]) VALUES (1, 3, 4, 1, N'OR592', 4)
INSERT [dbo].[Ordenes] ([Id], [Mesa], [IdProducto], [Cantidad], [Orden], [Estatus]) VALUES (2, 3, 2, 1, N'OR598', 5)
INSERT [dbo].[Ordenes] ([Id], [Mesa], [IdProducto], [Cantidad], [Orden], [Estatus]) VALUES (3, 3, 4, 1, N'OR592', 4)
INSERT [dbo].[Ordenes] ([Id], [Mesa], [IdProducto], [Cantidad], [Orden], [Estatus]) VALUES (4, 3, 4, 1, N'OR5945', 5)
INSERT [dbo].[Ordenes] ([Id], [Mesa], [IdProducto], [Cantidad], [Orden], [Estatus]) VALUES (5, 3, 4, 1, N'OR5097', 2)
INSERT [dbo].[Ordenes] ([Id], [Mesa], [IdProducto], [Cantidad], [Orden], [Estatus]) VALUES (6, 3, 4, 1, N'OR592', 4)
INSERT [dbo].[Ordenes] ([Id], [Mesa], [IdProducto], [Cantidad], [Orden], [Estatus]) VALUES (7, 3, 4, 1, N'OR592', 4)
INSERT [dbo].[Ordenes] ([Id], [Mesa], [IdProducto], [Cantidad], [Orden], [Estatus]) VALUES (8, 3, 4, 1, N'OR592', 4)
INSERT [dbo].[Ordenes] ([Id], [Mesa], [IdProducto], [Cantidad], [Orden], [Estatus]) VALUES (9, 3, 4, 1, N'OR592', 4)
INSERT [dbo].[Ordenes] ([Id], [Mesa], [IdProducto], [Cantidad], [Orden], [Estatus]) VALUES (10, 3, 4, 13, N'OR60', 4)
INSERT [dbo].[Ordenes] ([Id], [Mesa], [IdProducto], [Cantidad], [Orden], [Estatus]) VALUES (1004, 3, 12, 13, N'OR785', 3)
INSERT [dbo].[Ordenes] ([Id], [Mesa], [IdProducto], [Cantidad], [Orden], [Estatus]) VALUES (1005, 3, 12, 3, N'OR785', 3)
INSERT [dbo].[Ordenes] ([Id], [Mesa], [IdProducto], [Cantidad], [Orden], [Estatus]) VALUES (1006, 3, 12, 1, N'OR785', 3)
INSERT [dbo].[Ordenes] ([Id], [Mesa], [IdProducto], [Cantidad], [Orden], [Estatus]) VALUES (1007, 3, 3, 2, N'OR984', 1)
INSERT [dbo].[Ordenes] ([Id], [Mesa], [IdProducto], [Cantidad], [Orden], [Estatus]) VALUES (1008, 3, 3, 2, N'OR98878', 1)
INSERT [dbo].[Ordenes] ([Id], [Mesa], [IdProducto], [Cantidad], [Orden], [Estatus]) VALUES (1009, 21, 6, 3, N'OR988', 1)
INSERT [dbo].[Ordenes] ([Id], [Mesa], [IdProducto], [Cantidad], [Orden], [Estatus]) VALUES (1010, 24, 6, 2, N'OR999', 3)
INSERT [dbo].[Ordenes] ([Id], [Mesa], [IdProducto], [Cantidad], [Orden], [Estatus]) VALUES (1011, 3, 6, 3, N'OR5097', 2)
INSERT [dbo].[Ordenes] ([Id], [Mesa], [IdProducto], [Cantidad], [Orden], [Estatus]) VALUES (1012, 3, 5, 3, N'OR5097', 2)
SET IDENTITY_INSERT [dbo].[Ordenes] OFF
GO
SET IDENTITY_INSERT [dbo].[Productos] ON 

INSERT [dbo].[Productos] ([Id], [Sku], [Nombre], [Stock], [Minimo]) VALUES (1, N'PK123     ', N'Soda', 10, 2)
INSERT [dbo].[Productos] ([Id], [Sku], [Nombre], [Stock], [Minimo]) VALUES (2, N'PK1243    ', N'Burger', 10, 3)
INSERT [dbo].[Productos] ([Id], [Sku], [Nombre], [Stock], [Minimo]) VALUES (3, N'PK1243    ', N'Salsa', 11, 3)
INSERT [dbo].[Productos] ([Id], [Sku], [Nombre], [Stock], [Minimo]) VALUES (4, N'PK1243    ', N'Pizza', 3, 3)
INSERT [dbo].[Productos] ([Id], [Sku], [Nombre], [Stock], [Minimo]) VALUES (5, N'PK1278    ', N'Papas Fritas', 13, 2)
INSERT [dbo].[Productos] ([Id], [Sku], [Nombre], [Stock], [Minimo]) VALUES (6, N'PK1243    ', N'Alitas', 7, 3)
INSERT [dbo].[Productos] ([Id], [Sku], [Nombre], [Stock], [Minimo]) VALUES (7, N'PK1243    ', N'Hot-Dog', 15, 3)
INSERT [dbo].[Productos] ([Id], [Sku], [Nombre], [Stock], [Minimo]) VALUES (12, N'PK1278    ', N'Costillas BBQ', 0, 3)
INSERT [dbo].[Productos] ([Id], [Sku], [Nombre], [Stock], [Minimo]) VALUES (1002, N'PK1243    ', N'Pastel', 16, 2)
INSERT [dbo].[Productos] ([Id], [Sku], [Nombre], [Stock], [Minimo]) VALUES (1003, N'PK124358  ', N'Limones', 16, 2)
INSERT [dbo].[Productos] ([Id], [Sku], [Nombre], [Stock], [Minimo]) VALUES (1004, N'PK584     ', N'Flan Napolitano', 20, 3)
SET IDENTITY_INSERT [dbo].[Productos] OFF
GO
ALTER TABLE [dbo].[Ordenes]  WITH CHECK ADD  CONSTRAINT [FK_Ordenes_Ordenes] FOREIGN KEY([IdProducto])
REFERENCES [dbo].[Productos] ([Id])
GO
ALTER TABLE [dbo].[Ordenes] CHECK CONSTRAINT [FK_Ordenes_Ordenes]
GO
USE [master]
GO
ALTER DATABASE [Restaurante] SET  READ_WRITE 
GO
